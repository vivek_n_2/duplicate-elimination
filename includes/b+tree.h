#ifndef BPLUSTREE_H
#define BPLUSTREE_H

typedef unsigned long long int ull;

typedef struct __bplusnode
{
	int k;
	int nkeys;
	bool isleaf;
	ull *keys;
	struct __bplusnode **children;

}bplusnode;

class bplustree
{
	public:
		int degree;
		bplusnode *root;
		bplustree(int k);
		bplusnode* newnode(bool isleaf);
		void insert(ull key);
		bplusnode* insertutil(ull key, bplusnode* node, bplusnode* par, int idxinpar);
		bool isfull(bplusnode* node);
		bplusnode* splitnode(bplusnode* node,bplusnode* par,int idxinpar);
		void traverseutil(bplusnode* root);
		void traverse();
		bool searchutil(bplusnode* root,ull key);
		bool search(ull key);
};

#endif
