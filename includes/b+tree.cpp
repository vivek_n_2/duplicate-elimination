#include <bits/stdc++.h>
#include "b+tree.h"

using namespace std;

bplustree::bplustree(int k=3)
{
	degree = k;
	root = NULL;
}

bplusnode* bplustree::newnode(bool isleaf)
{
	bplusnode* node = (bplusnode*)malloc(sizeof(bplusnode));
	node->k = degree;
	node->nkeys = 0;
	node->isleaf = isleaf;
	node->keys = (ull*)malloc((2*degree-1)*sizeof(ull));
	node->children = (bplusnode**)malloc((2*degree)*sizeof(bplusnode*));
	for(int i=0;i<2*degree-1;i++)
		node->keys[i] = 0;
	for(int i=0;i<2*degree;i++)
		node->children[i] = NULL;
	return node;
}

bool bplustree::isfull(bplusnode* node)
{
	return node->nkeys == node->k;
}

bplusnode* bplustree::splitnode(bplusnode* node, bplusnode* par, int idxinpar)
{
	bplusnode* exnode=par;
	if(par==NULL)
	{
		par = newnode(false);
		par->children[0] = node;
	}
	if(node->isleaf)
	{
		int left=(node->nkeys/2);
		int right=node->nkeys-left;
		bplusnode *extranode = newnode(true);
		for(int idx=0;idx<right;idx++)
			extranode->keys[idx] = node->keys[left+idx];
		node->nkeys = left;
		extranode->nkeys = right;
		par->keys[par->nkeys] = extranode->keys[0];
		par->children[par->nkeys+1] = extranode;
		for(int idx=par->nkeys;idx>0 && par->keys[idx-1]>par->keys[idx];idx--)
		{
			swap(par->keys[idx],par->keys[idx-1]);
			swap(par->children[idx+1],par->children[idx]);
		}
		par->nkeys++;
	}
	else
	{
		int left=(node->nkeys/2);
		int right=node->nkeys-left;
		bplusnode *extranode = newnode(false);
		for(int idx=0;idx<left;idx++)
			extranode->keys[idx] = node->keys[right+idx];
		for(int idx=0;idx<=left;idx++)
			extranode->children[idx] = node->children[right+idx];
		node->nkeys = left;
		extranode->nkeys = left;
		par->keys[par->nkeys] = node->keys[left];
		par->children[par->nkeys+1] = extranode;
		for(int idx=par->nkeys;idx>0 && par->keys[idx-1]>par->keys[idx];idx--)
		{
			swap(par->keys[idx],par->keys[idx-1]);
			swap(par->children[idx+1],par->children[idx]);
		}
		par->nkeys++;
	}
	return par;
}

bplusnode* bplustree::insertutil(ull key, bplusnode* node, bplusnode* par,int idxinpar)
{
	if(node==NULL)
	{
		node=newnode(true);
		node->keys[node->nkeys++]=key;
	}
	else if(node->isleaf)
	{
		node->keys[node->nkeys] = key;
		for(int idx=node->nkeys;idx>0 && node->keys[idx-1]>node->keys[idx];idx--)
			swap(node->keys[idx],node->keys[idx-1]);
		node->nkeys++;
	}
	else
	{
		int idx;
		for(idx=0;idx<node->nkeys && node->keys[idx]<key;idx++);
		node->children[idx] = insertutil(key,node->children[idx],node,idx);
		if(isfull(node->children[idx]))
			node=splitnode(node->children[idx],node,idx);
	}
	return node;
}

void bplustree::insert(ull key)
{
	root = insertutil(key,root,NULL,0);
	if(isfull(root))
	{
		root = splitnode(root,NULL,0);
	}
}

void bplustree::traverseutil(bplusnode* root)
{
	if(root!=NULL)
	{
		if(root->isleaf)
		{
			for(int i=0;i<root->nkeys;i++)
				cout << root->keys[i] << " ";
			cout << " -> ";
		}
		else
		{
			for(int i=0;i<root->nkeys+1;i++)
				traverseutil(root->children[i]);
		}
	}
}

void bplustree::traverse()
{
	traverseutil(root);
	cout << "NULL" << endl;
}

bool bplustree::searchutil(bplusnode* root,ull key)
{
	if(root==NULL)
		return false;
	int idx;
	/* Without bsearch
	int idx;
	for(idx=0;idx<root->nkeys && root->keys[idx]<key;idx++);
	if(idx<root->nkeys && root->keys[idx]==key)
			return true;
	*/
	idx = upper_bound(root->keys, root->keys+root->nkeys, key)-root->keys; // gives idx where val > key
	if(idx>0 && root->keys[idx-1]==key)
		return true;
	return searchutil(root->children[idx],key);
}

bool bplustree::search(ull key)
{
	return searchutil(root,key);
}
