#ifndef MYHASH_H
#define MYHASH_H

typedef unsigned long long int ull;

ull gethash(std::string record);
std::string dehash(ull key);

#endif
