#include <bits/stdc++.h>
#include "hasher.h"

using namespace std;

hashtable::hashtable(void)
{
	myset.clear();
}

void hashtable::insert(ull key)
{
	myset.insert(key);
}

bool hashtable::search(ull key)
{
	return myset.find(key) != myset.end();
}
