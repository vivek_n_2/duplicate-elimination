#include <bits/stdc++.h>
#include "myhash.h"

using namespace std;

ull mul = 5001;

ull gethash(string record)
{
	ull hash = 0;
	ull curval;
	int len = record.size();
	for(int i=0;i<len;i++)
	{
		curval=0;
		while(i<len && record[i]!=',')
		{
			curval = curval*10 + record[i]-'0';
			i++;
		}
		hash = hash*mul + curval;
	}
	return hash;
}

string dehash(ull key)
{
	vector<string> res;
	res.clear();
	while(key)
	{
		std::stringstream str;
		str << (key%mul);
		res.push_back(str.str());
		key/=mul;
	}
	string ret = res[res.size()-1];
	for(int i=res.size()-2;i>=0;i--)
		ret+=','+res[i];
	return ret;
}
