#ifndef HASHER_H
#define HASHER_H

typedef unsigned long long int ull;

class hashtable
{
	public:
		std::set<ull> myset;
		hashtable();
		void insert(ull key);
		bool search(ull key);
};

#endif
