#ifndef BUFFERSIM_H
#define BUFFERSIM_H

using namespace std;

typedef unsigned long long int ull;

class buffermem 
{
	public:
		int numbuffers;
		int blocksize;
		ull** input;
		ull** output;
		int numinput;
		int numoutput;
		int inputsize;
		int outputsize;
		int readptr;
		buffermem(int M,int B);
		void clearinput();
		void clearoutput();
		bool inputisfull();
		bool outputisfull();
		void storeinput(ull key);
		void storeoutput(ull key);
		bool readinput(ull& ret);
		void flushoutput();
};

#endif
