#include <bits/stdc++.h>
#include "buffersim.h"
#include "myhash.h"

using namespace std;

buffermem::buffermem(int M,int B)
{
	numbuffers = M;
	blocksize = B;
	numinput = M-1;
	numoutput = 1;
	input = (ull**)malloc(numinput*sizeof(ull*));
	for(int i=0;i<numinput;i++)
		input[i] = (ull*)malloc(B*sizeof(ull));

	output = (ull**)malloc(numoutput*sizeof(ull*));
	for(int i=0;i<numoutput;i++)
		output[i] = (ull*)malloc(B*sizeof(ull));
	
	clearinput();
	clearoutput();
}

void buffermem::clearinput()
{
	inputsize = 0;
	readptr = 0;
}

void buffermem::clearoutput()
{
	outputsize = 0;
}

bool buffermem::inputisfull()
{
	return inputsize>=(numinput*blocksize);
}

bool buffermem::outputisfull()
{
	return outputsize>=(numoutput*blocksize);
}

void buffermem::storeinput(ull key)
{
	input[inputsize/blocksize][inputsize%blocksize] = key;
	inputsize++;
}

void buffermem::storeoutput(ull key)
{
	output[outputsize/blocksize][outputsize%blocksize] = key;
	outputsize++;
}

bool buffermem::readinput(ull& ret)
{
	if(readptr >= inputsize)
	{
		clearinput();
		return false;
	}
	ret = input[readptr/blocksize][readptr%blocksize];
	readptr++;
	return true;
}

void buffermem::flushoutput()
{
	for(int i=0;i<outputsize;i++)
		cout << dehash(output[i/blocksize][i%blocksize]) << endl;
	clearoutput();
}
