//Vivek Nynaru
#include <bits/stdc++.h>
#include "b+tree.h"
#include "myhash.h"
#include "hasher.h"
#include "buffersim.h"

using namespace std;

ifstream input;
string inputfile;   // Path to inputfile
int blocksize;      // size of blocksize in bytes
int numbuffers;     // Number of buffers available
bool isbtree;       // true if DS used is b+tree

void close()
{
	input.close();
}

void getnext()
{
	buffermem mainmem(numbuffers,blocksize/4);   // Initailizing main memory with specified buffers and number of records per block
	if(isbtree)
	{
		string line;
		int btreedegree = ((blocksize-8)/12)+1;     // 4*degree + 8*(degree+1) <= blocksize
		bplustree uniq(btreedegree);
		ull val;

		/*          Simulation of Main Memory
		 * Store the record into main memory till buffers get filled completely.
		 * If buffers are filled read record by record and store output into output buffer.
		 * When output buffers are filled flush the output
		 * Similar to the hash table too
		 */

		while(getline(input,line))
		{
			val = gethash(line);
			mainmem.storeinput(val);
			if(mainmem.inputisfull())
			{
				while(mainmem.readinput(val))
				{
					if(!uniq.search(val))
					{
						uniq.insert(val);
						mainmem.storeoutput(val);
						if(mainmem.outputisfull())
							mainmem.flushoutput();
					}
				}
			}
		}
		while(mainmem.readinput(val))
		{
			if(!uniq.search(val))
			{
				uniq.insert(val);
				mainmem.storeoutput(val);
				if(mainmem.outputisfull())
					mainmem.flushoutput();
			}
		}
		mainmem.flushoutput();
	}
	else
	{
		string line;
		hashtable uniq;
		ull val;
		while(getline(input,line))
		{
			val = gethash(line);
			mainmem.storeinput(val);
			if(mainmem.inputisfull())
			{
				while(mainmem.readinput(val))
				{
					if(!uniq.search(val))
					{
						uniq.insert(val);
						mainmem.storeoutput(val);
						if(mainmem.outputisfull())
							mainmem.flushoutput();
					}
				}
			}
		}
		while(mainmem.readinput(val))
		{
			if(!uniq.search(val))
			{
				uniq.insert(val);
				mainmem.storeoutput(val);
				if(mainmem.outputisfull())
					mainmem.flushoutput();
			}
		}
		mainmem.flushoutput();
	}
}

void open()
{
	input.open(inputfile.c_str());
	getnext();
}

int main(int argc, char* argv[])
{
	if(argc!=5)
	{
		cout << "Exactly 5 arguments must be passed." << endl;
		cout << "usage ./unique <inputfile> <blocksize> <numbuffers> <flag>" << endl;
		cout << "flag = 0 for btree" << endl;
		cout << "     = 1 for hashtable" << endl;
		return 0;
	}
	
	inputfile = argv[1];
	blocksize = atoi(argv[2]);
	numbuffers = atoi(argv[3]);
	int flag = atoi(argv[4]);
	isbtree = (flag==0);
	
	open();
	return 0;
}
