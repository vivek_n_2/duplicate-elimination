#! /usr/bin/env python

import random

numtests = random.randint(1,500000)
mintest = 1
maxtest = 5000
numattr = random.randint(1,3)
for test in xrange(numtests):
    print ','.join([str(random.randint(mintest,maxtest)) for i in xrange(numattr)])
