#! /bin/sh

g++ -o unique ../includes/b+tree.cpp ../includes/myhash.cpp ../includes/hasher.cpp ../includes/buffersim.cpp ../src/main.cpp -I ../includes/
minblocksize=32
maxblocksize=128
minbuffers=2
maxbuffers=20
numtests=30
if [ $? -eq 0 ]
then
	echo "Compilation Succesfull."
	for test in $(seq 1 $numtests); do
		blocksize=`shuf -i $minblocksize-$maxblocksize -n 1`
		numbuffers=`shuf -i $minbuffers-$maxbuffers -n 1`
		flag=`shuf -i 0-1 -n 1`
		python GenerateTests.py > testcases
		echo "Test $test : ./unique testcases $blocksize $numbuffers $flag "
		./unique testcases $blocksize $numbuffers $flag > myoutput
		python tester.py > actoutput
		result=$(diff myoutput actoutput)
		if [ $? -eq 0 ]
		then
			echo "$(tput setaf 2)Test $test Passed $(tput sgr0)"
		else
			echo "$(tput setaf 1)Test $test Failed $(tput sgr0)"
			echo "$result"
			break
		fi
		rm testcases
		rm myoutput
		rm actoutput
	done
	rm unique
else
	echo "Compilation Failed."
fi
